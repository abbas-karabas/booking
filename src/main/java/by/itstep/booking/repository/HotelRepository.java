package by.itstep.booking.repository;

import by.itstep.booking.entity.HotelEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface HotelRepository extends JpaRepository<HotelEntity, Integer> {

    @Query(value = "SELECT * FROM hotels WHERE deleted_at IS NULL", nativeQuery = true)
    List<HotelEntity> findAll();
}
