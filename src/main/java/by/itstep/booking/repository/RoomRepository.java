package by.itstep.booking.repository;

import by.itstep.booking.entity.RoomEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RoomRepository extends JpaRepository<RoomEntity, Integer> {

    @Query(value = "SELECT * FROM rooms WHERE deleted_at IS NULL", nativeQuery = true)
    List<RoomEntity> findAll();

}
