package by.itstep.booking.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "hotels")
@Where(clause = "deleted_at IS NULL")
public class HotelEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "address")
    private String address;

    @Column(name = "rating")
    private Integer rating;

    @Column(name = "comment")
    private String comment;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "stars")
    private HotelStars stars;

    @Column(name = "price")
    private String price;

    @Column(name = "deleted_at")
    private Instant deletedAt;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToMany(mappedBy = "country", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<HotelEntity> hotels = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "room_id")
    private RoomEntity room;






}
