package by.itstep.booking.entity;

import lombok.Data;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.Instant;

@Data
@Entity
@Table(name = "users")
@Where(clause = "deleted_at IS NULL")
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column (name = "phone")
    private String phone;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;

    @Column(name = "money")
    private Integer money;

    @Column(name = "blocked")
    private Boolean blocked;

    @Column(name = "deleted_at")
    private Instant deletedAt;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "role")
    private UserRole role;

    @ManyToOne
    @JoinColumn(name = "booking_id")
    private BookingEntity booking;



}
