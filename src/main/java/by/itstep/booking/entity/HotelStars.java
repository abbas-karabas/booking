package by.itstep.booking.entity;

public enum HotelStars {

    ONE, TWO, THREE, FOUR, FIVE
}
